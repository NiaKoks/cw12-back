const nanoid = require('nanoid');
const axios = require('axios');
const express = require('express');
const User = require('../models/User');
const config = require('../config')


const router = express.Router();

router.post('/',async (req,res)=>{
    const user = new User(req.body);
    user.generateToken();

    try{
        await user.save();
        return res.send({message: "User registred", user});
    } catch (e) {
        return res.status(400).send(e);
    }
});
router.post('/facebookLogin',async (req,res) =>{
    const inputToken = req.body.accessToken;
    const accessToken = config.facebook.appId + '|' + config.facebook.appSecret;

    const debugTokenUrl=`https://graph.facebook.com/debug_token?input_token=${inputToken}&access_token=${accessToken}`;

    try {
        const response = await axios.get(debugTokenUrl);

        if (response.data.data.error) {
            return res.status(401).send({message: 'Facebook token incorrect'});
        }
        if (req.body.id !== response.data.data.user_id) {
            return res.status(401).send({message: 'Wrong user ID'});
        }

        let user = await User.findOne({facebookId: req.body.id});

        if (!user) {
            user = new User({
                username: req.body.email,
                password: nanoid(),
                facebookId: req.body.id,
                displayName: req.body.name,
                avatar: req.body.picture.data.url
            });
        }
        user.generateToken();
        await user.save();

        return res.send({message: 'Login or register successful', user})
    } catch (error) {
        console.log(error);
        return res.status(401).send({message:'Facebook token incorrect'});
    }
});

router.post('/sessions', async (req, res) => {
    const user = await User.findOne({username: req.body.username});

    if (!user) {
        return res.status(400).send({error: 'User does not exist'});
    }

    const isMatch = await user.checkPassword(req.body.password);

    if (!isMatch) {
        return res.status(400).send({error: 'Password incorrect'});
    }

    user.generateToken();

    await user.save();

    res.send({message: 'Login successful', user});
});

router.delete('/sessions', async (req, res)=>{
    const token = req.get('Authorization');
    const success = {message: 'Ok'};


    if (!token) {
        return res.send(success);
    }

    const user = await User.findOne({token});

    if (!user) {
        return res.send(success);
    }

    user.generateToken();
    await user.save();

    res.send(success);

});

module.exports = router;