const express = require('express');
const path = require('path');
const auth = require('../middleware/auth');
const nanoid = require('nanoid');
const multer = require('multer');
const GalleryImage = require('../models/GalleryImage');
const config = require('../config');

const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, config.uploadPath);
    },
    filename: (req, file, cb) => {
        cb(null, nanoid() + path.extname(file.originalname));
    }
});

const upload = multer({storage});

const router = express.Router();

router.get('/',(req,res)=>{
    let criteria ={}

    GalleryImage.find(criteria).populate('user')
        .then(galleryImage => res.send(galleryImage))
        .catch(()=> res.sendStatus(500));
});

router.post('/',[auth,upload.single('image')],(req,res)=>{
    const galleryData = req.body;
    galleryData.user = req.user._id;

    if (req.file) {
        galleryData.image = req.file.filename;
    }
    const galleryImage = new GalleryImage(galleryData);
    galleryImage.save()
        .then(result => res.send(result))
        .catch(error => res.status(400).send(error));
});

router.delete('/:id', [auth],(req,res)=>{
    GalleryImage.remove({_id: req.params.id})
        .then(result=>res.send(result))
        .catch(error=>res.status(403).send(error))

});

module.exports = router;