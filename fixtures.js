const nanoid = require ("nanoid");
const mongoose = require('mongoose');

const GalleryImage =require('./models/GalleryImage');
const User =require('./models/User');
const config = require('./config');

const run =async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for(let collection of collections){
        await collection.drop();
    }

    const [user,user2] = await User.create(
        {   username:'Jonny Mnemonic',
            token: nanoid(),
            password: "123",
            displayName:'Jonny Mnemonic',
            facebookId: '102116221039848',
            avatar: null
        },
        {   username:'Eclipsa',
            token: nanoid(),
            password: "123",
            displayName:'Eclipsa',
            facebookId: '110726613502619',
            avatar: 'userpic.jpg'
        }
    );
   await GalleryImage.create(
        {
            title: "Queen",
            user:user._id,
            image: "queen.png",
            info: "Legends",
        },
        {
            title: "Queen",
            user:user._id,
            image: "queen.png",
            info: "Legends",
        },
        {
            title: "Queen",
            user:user2._id,
            image: "queen.png",
            info: null,
        }
    );

    return connection.close();

};


run().catch(error =>{
    console.log('Something wrong happened ...' ,error);
});