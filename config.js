const path = require('path');

const rootPath = __dirname;

module.exports = {
    rootPath,
    uploadPath: path.join(rootPath, 'public/uploads'),
    dbUrl: 'mongodb://localhost/photogallery',
    mongoOptions: {useNewUrlParser: true,useCreateIndex: true},
    facebook: {appId:'293173334897251',appSecret:'32d86ca261b262ceb0820ce17dd10978'}
};