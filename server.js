const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const users = require('./app/users');
const gallery = require('./app/gallery');

const app = express();
app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

mongoose.connect('mongodb://localhost/photogallery',{useNewUrlParser: true}).then(()=>{
    app.use('/users',users);
    app.use('/gallery',gallery);

    app.listen(port,()=> console.log(`Server runned on ${port} port`));
});